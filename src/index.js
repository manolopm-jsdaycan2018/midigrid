import React from 'react'
import ReactDOM from 'react-dom'
import Grid from './components/grid.js'

ReactDOM.render(<Grid />, document.getElementById('content'))
