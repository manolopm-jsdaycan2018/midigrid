'use strict'

import React from 'react'
import PixelGrid from './pixelgrid'

export default class Grid extends React.PureComponent {
  render () {
    return (
      <PixelGrid />
    )

  }
}

