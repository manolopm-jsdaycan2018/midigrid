# Midi Grid

Este es un proyecto de juguete que preparé para la JSDayCAN2018

Usa webmidi y react para interactuar con un Launchpad pro.

La idea es usar el launchpad en conjunto con la web. En el launchpad, dandole a double y circulo, se subdivide el grid en la web. 
Y en la web con el botón derecho se elige el color de cada subdivisión que se refleja en el launchpad.
Hay varios bugs, y por ahora solo funciona bien con divisiones potencias de 2.

Para probarlo está en linea aquí: https://www.graph-ic.org/react-experiments/midi-demos/midigrid/public/

Para usarlo en local:
- npm run build